const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: path.join(__dirname, "app", "app.js"),
    output: {
        path: path.join(__dirname, "dist"),
        filename: "app.bundle.js"
    },
    mode: process.env.NODE_ENV || "development",
    resolve: {
        alias: {
            // "react": process.env.NODE_ENV === "production" ? "preact-compat" : "react",
            // "react-dom": process.env.NODE_ENV === "production" ? "preact-compat" : "react-dom",
        },
        modules: [path.resolve(__dirname, "app"), "node_modules"],
    },
    devServer: {
        contentBase: path.join(__dirname, "app")
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({template: "app/index.html"})
    ],
    devServer: {
        disableHostCheck: true
    }
}