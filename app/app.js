import React from "react";
import ReactDOM from "react-dom";
import Button from "@material-ui/core/Button";
import { HashRouter } from "react-router-dom";
import SignIn from "./components/SignIn";
import Dashboard from "./Dashboard";

function App() {
    return(
        <Button variant="contained" color="primary">
            Hello World
        </Button>
    )
}

ReactDOM.render((
    <HashRouter>
        <Dashboard />
    </HashRouter>
), document.getElementById("root"));